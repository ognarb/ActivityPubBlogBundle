<?php
/**
 * This file is part of ActivityPubBlogBundle.
 *
 * Copyright Carl-Lucien Schwan
 *
 * ActivityPubBlogBundle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ActivityPubBlogBundle. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ActivityPubBlogBundle..  If not, see <https://www.gnu.org/licenses/>.
 */


namespace Ognarb\ActivityPubBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Article
 * This class is used to represent a article posted to the blog
 * @package ActivityPubBlogBundle\Entity
 * @author  Carl-Lucien Schwan <schwancarl@protonmail.com>
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Article
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Title
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * Content (in markdown)
     * @var string
     * @ORM\Column(name="content", type="text")
     */
    private $content;
}