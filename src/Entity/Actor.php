<?php

/**
 * This file is part of ActivityPubBlogBundle.
 *
 * Copyright Carl-Lucien Schwan
 *
 * ActivityPubBlogBundle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ActivityPubBlogBundle. is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ActivityPubBlogBundle..  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Ognarb\ActivityPubBlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Actor
 * This class is used to represent a actor
 * @package ActivityPubBlogBundle\Entity
 * @see https://www.w3.org/TR/activitypub/#actor-objects
 * @author  Carl-Lucien Schwan <schwancarl@protonmail.com>
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Actor {
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Actor type
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#actor-types
     * @var integer
     * @ORM\Column(name="actorType", type="integer")
     */
    private $actorType;
    public const APPLICATION = 0;
    public const GROUP = 1;
    public const ORGANIZATION = 2;
    public const PERSON = 3;
    public const SERVICE = 4;

    /**
     * Collection of actors that this actor is following
     * @var
     * @see https://www.w3.org/TR/activitypub/#following
     */
    private $following;

    /**
     * Collection of actors that follow this actor
     * @var
     * @see https://www.w3.org/TR/activitypub/#followers
     */
    private $followers;

    /**
     * Collection of object this actor has liked
     * @var
     * @see https://www.w3.org/TR/activitypub/#liked
     */
    private $liked;

    /**
     * A short username with no uniqueness guarantees
     * @var string
     * @see https://www.w3.org/TR/activitypub/#actor-objects
     * @ORM\Column(name="content", type="text")
     */
    private $preferedUsername;

    /**
     * The preferred "nickname" of the actor
     * @var string
     */
    private $name;

    /**
     * A quick summary or bio
     * @var string
     */
    private $sumary;

    // private $icon; TODO
}

