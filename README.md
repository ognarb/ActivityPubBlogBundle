# ActivityPub blog bundle for Symfony 4

**WIP** Don't work yet

## Goal

Provide a nice blog bundle for Symfony4 with a ActivityPub integration. Making it posible to allow any existing Mastodon, Peertube, GNU Social users to follow and interact with your blog.

## Contributing

Any help is welcomme, even more if you have some experience with the activitypub protocol or making symfony bundle. Any contribution is release under the term of the LGPL. 

## Installation

### Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```console
$ composer require ognarb/activitypub-blog-bundle
```

### Applications that don't use Symfony Flex

#### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require ognarb/activitypub-blog-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

#### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new ognarb/activitypub-blog-bundle/ActivityPubBlogBundle(),
        );

        // ...
    }

    // ...
}
```

## Some link and succesful activitypub project

+ [Activitypub doc](https://www.w3.org/TR/activitypub/) from the W3C
+ [Mastodon](https://github.com/tootsuite/mastodon)
+ [Peertube](https://github.com/Chocobozzz/PeerTube)

## LICENCE

This project is licenced under the LGPL.
